package Dockertest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
public class DockerChrome {


    public static void main(String[] args) throws MalformedURLException, InterruptedException {

        DesiredCapabilities dc = DesiredCapabilities.chrome();
        URL url = new URL("http://localhost:4444/wd/hub");
        RemoteWebDriver driver = new RemoteWebDriver(url,dc);
        driver.get("https://www.google.com/");
        System.out.println("title Page"+driver.getTitle());

        WebElement search = driver.findElementByXPath("//input[@class='gLFyf gsfi']");
        search.sendKeys("docker aya *_^");
        Thread.sleep(3000);
        driver.quit();

    }

}

